package com.baeldung;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class RandomMusicPlayer {

    public static void main(String[] args) {
        // Chemin des trois dossiers de musique
        String[] folders = {
                "C:/Users/ext22/Afpa_Docs/tmpRandom/dossier1",
                "C:/Users/ext22/Afpa_Docs/tmpRandom/dossier2",
                "C:/Users/ext22/Afpa_Docs/tmpRandom/dossier3"};

        // Nombre de lectures aléatoires pour chaque dossier
        int[] plays = {
                3,
                2,
                1};

        // Parcourir chaque dossier
        for (int i = 0; i < folders.length; i++) {
            File folder = new File(folders[i]);
            if (folder.exists() && folder.isDirectory()) {
                File[] files = folder.listFiles();
                if (files != null && files.length > 0) {
                    List<File> fileList = new ArrayList<>();
                    Collections.addAll(fileList, files);

                    // Mélanger la liste de fichiers
                    Collections.shuffle(fileList);
                    for (int k = 0; k < plays[i]; k++) {
                        // Renommer les fichiers avec des noms aléatoires
                        for (int j = 0; j < fileList.size(); j++) {
                            File file = fileList.get(j);
                            String extension = getFileExtension(file.getName());
                            String name = file.getName().substring(0, file.getName().lastIndexOf("."));
                            String randomName = generateRandomName() + name + "." + extension;
                            File newFile = new File(folder.getPath() + File.separator + randomName);
                            file.renameTo(newFile);
                            System.out.println("Fichier renommé : " + file.getName() + " -> " + newFile.getName());
                        }
                    }
                }
            }
        }
    }


    // Générer un nom de fichier aléatoire
    private static String generateRandomName() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        int length = 8; // Longueur du nom de fichier aléatoire
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (int i = 0; i < length; i++) {
            sb.append(characters.charAt(random.nextInt(characters.length())));
        }
        return sb.toString();
    }

    // Obtenir l'extension du fichier
    private static String getFileExtension(String fileName) {
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex != -1 && lastDotIndex < fileName.length() - 1) {
            return fileName.substring(lastDotIndex + 1);
        }
        return "";
    }
}